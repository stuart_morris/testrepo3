//
//  main.m
//  TestRepo3
//
//  Created by Stuart Morris on 12/04/2013.
//  Copyright (c) 2013 Stuart Morris. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
